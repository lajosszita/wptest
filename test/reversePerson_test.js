var assert = require('assert');
var request = require('supertest');
var should = require('should');

var ReversePerson = require('../wpAPI/reversePerson.js');

describe('Reverse person', function(){

	var apiKey = 'ff9525aab2e30a55a9d888f99d541515';
	this.timeout(8000);

	var row1 = {Name : 'J Hendrix', Address : 'Washington', Phone : ''};
	describe('1 should fill: ', function(){

		it(JSON.stringify(row1), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row1;
			var reversePerson = new ReversePerson(mandatoryHeaders, hPairs, apiKey);
			reversePerson.fill(row, function(filled_row){
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			});
		});
	
	});

	var row2 = {Name : 'Jane Smith', Address : '', Phone : ''};
	describe('2 should fill: ', function(){

		it(JSON.stringify(row2), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row2;
			var reversePerson = new ReversePerson(mandatoryHeaders, hPairs, apiKey);
			reversePerson.fill(row, function(filled_row){
				console.log(filled_row);
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			});
		});
	
	});

	var row3 = {Name : 'Kovács István', Address : '', Phone : ''};
	describe('3 should not be able to fill: ', function(){

		it(JSON.stringify(row3), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row3;
			var reversePerson = new ReversePerson(mandatoryHeaders, hPairs, apiKey);
			reversePerson.fill(row, function(filled_row){
				console.log(filled_row);
				done();
			});
		});
	
	});

});