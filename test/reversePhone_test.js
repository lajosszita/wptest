var assert = require('assert');
var request = require('supertest');
var should = require('should');

var ReversePhone = require('../wpAPI/reversePhone.js');

describe('Reverse phone', function(){

	var apiKey = 'ff9525aab2e30a55a9d888f99d541515';
	this.timeout(8000);
	//whitePagesPhone = '206-973-5100';
	//dramaticNumberPhone = '646-480-6649';

	var row1 = {Name : '', Address : '', Phone : '206-973-5100'};
	describe('1 should fill: ', function(){

		it(JSON.stringify(row1), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row1;
			var reversePhone = new ReversePhone(mandatoryHeaders, hPairs);
			reversePhone.fill(row, function(filled_row){
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			});
		});
	
	});

	var row2 = {Name : '', Address : 'Ashland MT 59004', Phone : '646-480-6649'};
	describe('2 should fill: ', function(){

		it(JSON.stringify(row2), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row2;
			var reversePhone = new ReversePhone(mandatoryHeaders, hPairs, apiKey);
			reversePhone.fill(row, function(filled_row){
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			});
		});
	
	});

	var row3 = {Name : 'Whitepages', Address : '', Phone : '206-973-5100'};
	describe('3 should fill: ', function(){

		it(JSON.stringify(row3), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row3;
			var reversePhone = new ReversePhone(mandatoryHeaders, hPairs, apiKey);
			reversePhone.fill(row, function(filled_row){
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			});
		});
	});

	var row4 = {Name : '', Address : '', Phone : '22'};
	describe('4 should not be bale to fill: ', function(){

		it(JSON.stringify(row4), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row4;
			var reversePhone = new ReversePhone(mandatoryHeaders, hPairs, apiKey);
			reversePhone.fill(row, function(filled_row){
				done();
			});
		});
	});

});