var assert = require('assert');
var request = require('supertest');
var should = require('should');

//app should be running at localhost:3000 for these tests
describe('rest service', function() {

	var url = 'http://localhost:3000';
	this.timeout(10000);

	describe.only('POST /upload', function () {    
		it('should send error (1) when no attachment is present', function (done) {
  		
	  		request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.error_code.should.equal(1);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		});

		it('should send error (1) when the content type is not multipart/form-data', function (done) {
  		
	  		request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'text/html')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.error_code.should.equal(1);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		});
	
		it('should send back a csv file, if valid csv file was sent with the request', function ( done ) {

			request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
	  		//.field('api_key', '')
	  		.attach('data_file', './test/testfiles/1ok.csv')
			.expect(200, done);
		
		});
	
		it('should send back an error (2), if 1 line csv file was sent with the request', function ( done ) {

			request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
	  		.attach('data_file', './test/testfiles/2error.csv')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.error_code.should.equal(2);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		
		});


		it('should send back an error (2), if malformed csv file was sent with the request', function ( done ) {

			request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
	  		.attach('data_file', './test/testfiles/3error.csv')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		
		});

		it('should send back an error (3), if duplicated header was sent in the csv with the request', function ( done ) {

			request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
	  		.attach('data_file', './test/testfiles/4error.csv')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.error_code.should.equal(3);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		
		});

		it('should send back an error (3), if uninterpretable header was sent in the csv with the request', function ( done ) {

			request(url)
	  		.post('/upload')
	  		.set('Content-Type', 'multipart/form-data')
	  		.attach('data_file', './test/testfiles/5error.csv')
			.expect(400)
	  		.end(function(err, res) {
		        if (err) { throw err; }
		        res.body.status.should.equal(400);
		        res.body.error_code.should.equal(3);
		        res.body.should.have.property('error');
		        res.body.should.have.property('hint');
		        done();
		    });
		
		});

	});
});