var assert = require('assert');
var request = require('supertest');
var should = require('should');

var ReverseBusiness = require('../wpAPI/reverseBusiness.js');

describe('Reverse business', function(){

	var apiKey = 'ff9525aab2e30a55a9d888f99d541515';
	this.timeout(5000);
	
	var row1 = {Name : 'Apple Inc', Address : 'San Francisco CA', Phone : ''};
	describe('should fill: ', function(){

		it(JSON.stringify(row1), function(done){

			var hPairs = {Phone : 'Phone', Name : 'Name', Address : 'Address'};
			var mandatoryHeaders = ['Phone','Name','Address'];

			var row = row1;
			var reverseBusiness = new ReverseBusiness(mandatoryHeaders, hPairs, apiKey);
			reverseBusiness.fill(row, function(filled_row){
				filled_row.Name.should.not.eql('');
				filled_row.Address.should.not.eql('');
				filled_row.Phone.should.not.eql('');
				done();
			
			});
		});
	
	});

});