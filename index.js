var express = require('express');
var multer  = require('multer');
var core = require('./core.js');
var upload = multer({ dest: 'uploads/' }).single('data_file');

var app = express();

app.get('/', function (req, res) {
	res.send('app ready! https://bitbucket.org/lajosszita/wptest/');
});

app.post('/upload', function (req, res) {

	if(!req.is('multipart/form-data')){
		console.log('error in request type');
		sendFileError(res);
		return;
	}

	upload(req, res, function (err) {

	    if (err){
	    	console.log('error in file upload:' + err);
	    	sendFileError(res);
    		return;
    	}
    	
    	core.processFile(req.file.filename, req.file.originalname, req.body.api_key, function(err, data){
    		
    		if(err){
    			console.log('error in parsing:' + err);
		    	if(err == 'invalid_format') sendParseError(res);
	    		if(err == 'invalid_headers') sendHeaderError(res);		
    			return;
    		}
    		
       		var sendOptions = {
			    root: __dirname + '/downloads/',
			    dotfiles: 'deny',
			    headers: {
			        'x-timestamp': Date.now(),
			        'x-sent': true
			    }
			};

			var filledFile = 'filled_' + req.file.originalname;
			res.sendFile(filledFile, sendOptions);
    	
    	});
	});
});

function sendFileError(res){
	res.status(400).json({ 
		status: 400 , 
		error : 'No csv file found!', 
		error_code : 1,
		hint : 'correct use of the API: POST data_file:<csv file> (mandatory), api_key:<api key> (optional)'
	});
};

function sendParseError(res){
	res.status(400).json({ 
		status: 400 , 
		error : 'Error parsing CSV!',
		error_code : 2, 
		hint : 'check if at least 2 lines are presen in the file,' +
		' and there are the same amount of columns in every row'
	});
};

function sendHeaderError(res){
	res.status(400).json({ 
		status: 400 , 
		error_code : 3,
		error : 'Error processing CSV!', 
		hint : 'check if header of the file contains only one Address, Name and Phone fileds.' +
		' Minor typos and small column name differencies eg. "Phone number" or "Phone#" instead of ' +
		'"phone" are tolerated'
	});
};



app.set('port', (process.env.PORT || 3000));

var server = app.listen(app.get('port'), function () {
	console.log('Node app is running on port', app.get('port'));
});