var https = require('https');
var qs = require("querystring");
var FuzzySet = require('fuzzyset.js');
var settings = require('../settings.js');
var addrParser = require('parse-address');

var method = ReverseBusiness.prototype;

function ReverseBusiness(mandatoryHeaders, hPairs, apiKey) {
	this.mandatoryHeaders = mandatoryHeaders;
    this.hPairs = hPairs;
    this.apiKey = (typeof apiKey != 'undefined') ? apiKey : settings.apiKey;
}

method.fill = function(row, cb){
	var that = this;
	
	var reqParams = {
		name : getValue(row,'Name'),
		page_first : 1,
		page_len : 1,
		api_key : this.apiKey
	};
	
	/* 
	 * Since no node module (parse-address, SnailMailAddressParser)
	 * seems to properly parse an addres, I have created my own function 
	 * which is really primitive, but at least (hopefuly) returns the city from the address
	 * so the API request can have the extra city parameter
	 * @TODO : create an address parsing method/module and add as many parameters to request as possible
	 */

	var parsedAddress = parseAddress(getValue(row,'Address'));
	reqParams.city = (parsedAddress.city) ? parsedAddress.city : getValue(row, 'Address');

	var queryString = qs.stringify(reqParams);
	var options = {
		host: 'proapi.whitepages.com',
		path: '/2.1/business.json?'+ queryString,
		method: 'GET'
	};

	/*var rand = Math.floor(Math.random() * 3) + 1;
	setTimeout(function(){
		console.log('CALL' + rand);
		cb(row);
	},rand*1000);*/
	
	var resBuffer = '';
	var req = https.request(options, function(res) {
		
		res.setEncoding('utf8');

		res.on('data', function(d) {
			resBuffer += d;
		});
		
		res.on('end', function(){
			procResponse(row,resBuffer,function(err, ret){
				if(err) {throw err};
				insert(row,'Phone', ret.Phone);
				cb(row);	
			});
		});
	});
	
	req.end();

	req.on('error', function(e) {
		console.error(e); //handle error!
		cb(row);
	});	


	function procResponse(row,response,cb){
		try{
			
			var ret = { Phone : ''};
			var res = JSON.parse(response);

			if(res.results && res.results.length > 0){
				console.log('ADSADA');
				var firstResult = res.results[0];
				//set phone number if available (first 'phones')
				if(firstResult.phones && firstResult.phones.length > 0){
					ret.Phone = firstResult.phones[0].phone_number;
					console.log('ret phone:'  + ret.Phone);
				}

			}
			cb(null,ret);
		}
		catch(err){
			console.log(err.message);
			cb(err,null);
		}
	}


	/** 
	 *	helpers 
	 *	@TODO: create utils class for utility methods 
	 */
	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function insert(row,key,value){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		row[userHeader] = value;
	}

	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function isEmptyColumn(row, key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		return (row[userHeader].length === 0);
	}

	/*
	*  quick and dirty address parsing
	*  @TODO: create working address parsing module
	*/
	function parseAddress(string){

		var exploded = string.split(',');
		var re = /^[A-Za-z ]+$/;
		var ret = {};

		exploded.forEach(function(s){
			if(re.test(s)
				&& s.toLowerCase().indexOf("street") == -1 
				&& s.toLowerCase().indexOf('str') == -1){
				ret.city = s;
			}
		});
		return ret;
	}
}

module.exports = ReverseBusiness;