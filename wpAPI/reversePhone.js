var https = require('https');
var qs = require("querystring");
var FuzzySet = require('fuzzyset.js');
var settings = require('../settings.js');
var addrParser = require('parse-address');

var method = ReversePhone.prototype;


function ReversePhone(mandatoryHeaders, hPairs, apiKey) {
	this.mandatoryHeaders = mandatoryHeaders;
    this.hPairs = hPairs;
    this.apiKey = (typeof apiKey != 'undefined') ? apiKey : settings.apiKey;
}

method.fill = function(row, cb){
	var that = this;
	
	var queryString = qs.stringify({
		phone : getValue(row,'Phone'),
		api_key : this.apiKey
	}); 

	var options = {
		host: 'proapi.whitepages.com',
		path: '/2.1/phone.json?'+ queryString,
		method: 'GET'
	};

	var resBuffer = '';
	/*
	var rand = Math.floor(Math.random() * 3) + 1;
	setTimeout(function(){
		console.log('CALL' + rand);
		cb(row);
	},rand * 1000);
	*/
	var req = https.request(options, function(res) {
		
		res.setEncoding('utf8');

		res.on('data', function(d) {
			resBuffer += d;
		});
		
		res.on('end', function(){
			procResponse(row,resBuffer,function(err, ret){ //Handle Error!
				if(err) {throw err};

				insert(row,'Name', ret.Name);
				insert(row,'Address', ret.Address);
				cb(row);	
			});
		});
	});
	
	req.end();

	req.on('error', function(e) {
		console.error(e); //handle error!
	});	

	function procResponse(row,response,cb){
		try{
			
			var ret = { Name : '', Address : ''};
			var res = JSON.parse(response);

			// phone number AND address is present
			// here I assume that if the address of the given row and the city of the
			// returned row partialy matches, than its the right person/business entity 
			if(isEmptyColumn(row,'Name') && !isEmptyColumn(row,'Address')){
				ret.Address = getValue(row,'Address');
				var givenAddress = addrParser.parseLocation(getValue(row,'Address'));
				if(givenAddress){
					var city = (givenAddress.city) ? givenAddress.city : getValue(row,'Address');
					//prepare fuzzy set to compare city
					var addressSet = FuzzySet([city]); 
					if(res.results && res.results.length > 0){
						//iterate results
						for(var i = 0; i < res.results.length; i++){
							var currentResult = res.results[i];
							//iterate belongs_to 
							for(var j = 0; j < currentResult.belongs_to.length; j++){
								if(currentResult.belongs_to[j].locations[0]
									&& addressSet.get(currentResult.belongs_to[j].locations[0].city)){
									var entity = currentResult.belongs_to[j];
									ret.Name = (entity.name) ? entity.name : entity.best_name;
								}
							}
						}
					}
				}
			}


			// phone number AND name is present
			// here I assume that if the name of the given row and the name or best_name of the
			// returned row partialy matches, than its the right location entity 
			if(isEmptyColumn(row,'Address') && !isEmptyColumn(row,'Name')){
				ret.Name = getValue(row,'Name');
				
				//prepare fuzzy set to compare name
				var nameSet = FuzzySet([ret.Name]); 
				if(res.results && res.results.length > 0){
					//iterate results
					for(var i = 0; i < res.results.length; i++){
						var currentResult = res.results[i];
						//iterate belongs_to 
						for(var j = 0; j < currentResult.belongs_to.length; j++){
							if(currentResult.belongs_to[j].locations.length > 0){
								var entity = currentResult.belongs_to[j];
								var name = (entity.name) ? entity.name : entity.best_name;
								if(nameSet.get(name)) 
									ret.Address = entity.locations[0].standard_address_location;
							}
						}
					}
				}
			}


			// Only phone number is present, other rows should be filled
			else{
				if(res.results && res.results.length > 0){
					var firstResult = res.results[0];
					//set name (first 'belongs to')
					if(firstResult.belongs_to && firstResult.belongs_to.length > 0){
						var firstBelongsTo = firstResult.belongs_to[0]; 
						ret.Name = (firstBelongsTo.name) ? firstBelongsTo.name : firstBelongsTo.best_name;
					}

					//set location (best location)
					if(firstResult.best_location){
						ret.Address = firstResult.best_location.standard_address_location;
					}
				}
			}

			cb(null,ret);
		}
		catch(err){
			console.log(err.message);
			cb(err,null);
		}
	}


	/** 
	 *	helpers 
	 *	@TODO: create utils class for utility methods
	 */
	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function insert(row,key,value){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		row[userHeader] = value;
	}

	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function isEmptyColumn(row, key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		return (row[userHeader].length === 0);
	}
}

module.exports = ReversePhone;