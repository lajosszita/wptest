var https = require('https');
var qs = require("querystring");
var FuzzySet = require('fuzzyset.js');
var settings = require('../settings.js');
var addrParser = require('parse-address');

var method = ReversePerson.prototype;


function ReversePerson(mandatoryHeaders, hPairs, apiKey) {
	this.mandatoryHeaders = mandatoryHeaders;
    this.hPairs = hPairs;
    this.apiKey = (typeof apiKey != 'undefined') ? apiKey : settings.apiKey;
}

method.fill = function(row, cb){
	var that = this;
	var reqParams = {
		name : getValue(row,'Name'),
		page_first : 1,
		page_len : 1,
		api_key : this.apiKey
	};
	
	/* 
	 * Since no node module (parse-address, SnailMailAddressParser)
	 * seems to properly parse an addres, I have created my own function 
	 * which is really primitive, but at least (hopefuly) returns the city from the address
	 * so the API request can have the extra city parameter
	 * @TODO : create an address parsing method/module and add as many parameters to request as possible
	 */
	if(!isEmptyColumn(row,'Address')){
		var parsedAddress = parseAddress(getValue(row,'Address'));
		reqParams.city = (parsedAddress.city) ? parsedAddress.city : getValue(row, 'Address');
	}
 
	var queryString = qs.stringify(reqParams);
	console.log(queryString);
	var options = {
		host: 'proapi.whitepages.com',
		path: '/2.1/person.json?'+ queryString,
		method: 'GET'
	};

	/*var rand = Math.floor(Math.random() * 3) + 1;
	setTimeout(function(){
		console.log('CALL' + rand);
		cb(row);
	},rand*1000);*/
	var resBuffer = '';
	var req = https.request(options, function(res) {
		
		res.setEncoding('utf8');

		res.on('data', function(d) {
			resBuffer += d;
		});
		
		res.on('end', function(){
			procResponse(row,resBuffer,function(err, ret){
				if(err) {throw err};

				insert(row,'Phone', ret.Phone);
				insert(row,'Address', ret.Address);
				cb(row);	
			});
		});
	});
	
	req.end();

	req.on('error', function(e) {
		console.error(e); //handle error!
		cb(row);
	});	


	function procResponse(row,response,cb){
		try{
			
			var ret = { Address : getValue(row, 'Address') , Phone : ''};
			var res = JSON.parse(response);

			if(res.results && res.results.length > 0){
				var firstResult = res.results[0];
				//set phone number if available (first 'phones')
				if(firstResult.phones && firstResult.phones.length > 0){
					ret.Phone = firstResult.phones[0].phone_number;
				}

				//set location if needed (best location)
				if(isEmptyColumn(row,'Address')){
					if(firstResult.best_location){
						ret.Address = firstResult.best_location.standard_address_location;
					}
				}
				else{
					ret.Address = getValue(row,'Address');
				}	
			}
			cb(null,ret);
		}
		catch(err){
			console.log(err.message);
			cb(err,null);
		}
	}


	/** 
	 *	helpers 
	 *	@TODO: create utils class for utility methods 
	 */
	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function insert(row,key,value){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		row[userHeader] = value;
	}

	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}

	function isEmptyColumn(row, key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		return (row[userHeader].length === 0);
	}

	/*
	*  quick and dirty address parsing
	*  @TODO: create working address parsing module
	*/
	function parseAddress(string){

		var exploded = string.split(',');
		var re = /^[A-Za-z ]+$/;
		var ret = {};

		exploded.forEach(function(s){
			if(re.test(s)
				&& s.toLowerCase().indexOf("street") == -1 
				&& s.toLowerCase().indexOf('str') == -1){
				ret.city = s;
			}
		});
		return ret;
	}
}

module.exports = ReversePerson;