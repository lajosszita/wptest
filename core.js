var fs = require('fs');
var Parser = require('papaparse');
var FuzzySet = require('fuzzyset.js');
var CSVFiller = require('./csvUtils/csvFiller.js');
var csvWriter = require('./csvUtils/csvWriter.js');


/** 
 * hPairs is an object which is used to store mandatory header : user header pairs
 * eg. { Phone : 'Phone number', Name : 'Full Name', Address : 'Ardess' } 
 * if it does not contain all 3 mandatory keys after filled, then the CSV is invalid
 */
var hPairs = {};

var mandatoryHeaders = ['Phone', 'Name', 'Address'];  

/**
 * Reads the file into a string which can be passed to parser object to 
 * parse it as CSV and breaks it down into [[header1 : value1, header2 : value2 ...] ...] 
 * format and also validates it. 
 */
module.exports.processFile = function(filename, originalName, apiKey ,cb) {

	var filePath = './uploads/' + filename;
	var encoding = 'utf8';

	fs.readFile(filePath, encoding , function (err, file) {
		if (err) throw err;
		
		Parser.parse(file, { complete : function(csv){

			hPairs = {};

			if (!isValidCSV(csv)){ 
				cb("invalid_format"); 
				return;
			}
			
			setHeaders(csv);
			if (!allHeadersMatched()){ 
				cb("invalid_headers"); 
				return;
			}
			
			csvFiller = new CSVFiller(mandatoryHeaders, hPairs, csv, apiKey);
			csvFiller.fill(function(filled){
				csvWriter.write(filled, originalName, function(){
					cb(null,filled);	
				});
			});	
			}, header: true
		});
	});
};

/**
 * The parser library comes with handy features eg. error, meta properties 
 * on the csv object, it is more convenient to validate the csv 
 * using these attributes
 */
function isValidCSV(csv){
	return (csv.errors.length === 0 
			&& csv.meta.fields.length >= 1 
			&& csv.data.length >= 1);
};

/**
 * Sets the hPairs object, documented above
 */
function setHeaders(csv){

	var csvHeaders = csv.meta.fields;
	var headerSet = FuzzySet(csvHeaders);

	for (var i = 0; i < mandatoryHeaders.length; i++) { 
		var res = headerSet.get(mandatoryHeaders[i]); 
		if(res != null)
			hPairs[mandatoryHeaders[i]] = res[0][1];
	};
	//console.log(hPairs);
};

/**
 * Checks if all mandatory fields are present as keys in hPairs object
 */
function allHeadersMatched(){
	return (mandatoryHeaders[0] in hPairs 
			&& mandatoryHeaders[1] in hPairs 
			&& mandatoryHeaders[2] in hPairs);
}




