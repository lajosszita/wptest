# WP test#
https://wp-csv-filler.herokuapp.com/

### Requirements to run the app: ###
* node.js (version 4.0.0 +) [nodejs.org](https://nodejs.org)
* npm (comes with node.js)
* mocha for testing (npm install mocha -g)

run: 
1. npm install
2. node index.js

test: 
1. start the app
2. mocha (from the project root)

### Using the deployed app###

* IMPORTANT: since heroku free tier puts apps to sleep if they were inactive for 2+ hours, please send a GET request to https://wp-csv-filler.herokuapp.com/ to 'wake up' the app

### REST API documentation ###

* Name : Upload
* Path : /upload
* Method : POST
* URL params : -
* Request body : 
data_file (the csv file) mandatory, api_key (custom api key) optional
* Success Response: Code : 200 and filled csv file

use multipart/form-data content type for request