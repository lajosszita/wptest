var Person = require('../model/person.js');
var Address = require('../model/address.js');
var Phone = require('../model/phone.js');
var Phone = require('../model/phone.js');

var ReversePhone = require('../wpAPI/reversePhone.js');
var ReversePerson = require('../wpAPI/reversePerson.js');
var ReverseBusiness = require('../wpAPI/reverseBusiness.js');


var method = CSVFiller.prototype;

/**
 *
 *
 */
function CSVFiller(mandatoryHeaders, hPairs, csv, apiKey) {
    this.hPairs = hPairs;
    this.csv = csv;
    this.mandatoryHeaders = mandatoryHeaders;
    this.reversePhone = new ReversePhone(mandatoryHeaders, hPairs, apiKey);
    this.reversePerson = new ReversePerson(mandatoryHeaders, hPairs, apiKey); 
	this.reverseBusiness = new ReverseBusiness(mandatoryHeaders, hPairs, apiKey);    
}

/**
 * @TODO document csv format change!
 */
method.fill = function(cb){

	var ready = 0;
	var total = this.csv.data.length;
	var filled = { headers : this.csv.meta.fields , data : [] };
	var that = this;
	
	for (var i = 0; i < this.csv.data.length; i++) {
		fillRow(this.csv.data[i],i);
	};

	
	function fillRow(row, index){
		var that = this;
		callAPI(row,function(filledRow){
			filled.data[index] = filledRow;
			ready += 1;
			if(ready === total) {
				cb(filled);
			}
		});
	}


	/**
	*
	*/
	function callAPI(row, cb){
		// phone num + 1 missing => call fillByPhone() using reversePhone
		if(!isEmptyColumn(row,'Phone') && (isEmptyColumn(row,'Address') || isEmptyColumn(row,'Name'))){
			console.log('RevPhone');
			that.reversePhone.fill(row, function(filled_row){
				cb(filled_row);
			});
		}
		// phone is missing but name is present => call fillByName using reversePerson
		else if(isEmptyColumn(row,'Phone') && !isEmptyColumn(row,'Name')){
			that.reversePerson.fill(row, function(filled_row){
				console.log('RevPerson');
				//if no phone was found by reversePerson and address is present, call reverseBusiness
				if(isEmptyColumn(row,'Phone') && !isEmptyColumn(row,'Address')){
					console.log('RevBusiness');
					that.reverseBusiness.fill(row, function(filled_row){
						cb(filled_row);						
					});
				}
				else{
					cb(filled_row);
				}
			});
		}


		// all columns are filled, or all are empty => do nothing
		else cb(row);
	}


	/** 
	 *	helpers 
	 *	@TODO: create utils class for utility methods 
	 */
	function isEmptyColumn(row, key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		return (row[userHeader].length === 0);
	}

	function insert(row,key,value){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';
		
		var userHeader = that.hPairs[key];
		row[userHeader] = value;
	}

	function getValue(row,key){
		if(that.mandatoryHeaders.indexOf(key) === -1) 
			throw key + 'not found in mandatory headers!';

		var userHeader = that.hPairs[key];
		return row[userHeader];
	}
} 


module.exports = CSVFiller;
