var fs = require('fs');

module.exports.write = function(csv, name, cb){

	var writer = fs.createWriteStream('./downloads/filled_' + name);

	writer.on('error', function(err) { throw err;});
	
	//headers
	writer.write(csv.headers[0]);
	for(var i = 1; i < csv.headers.length; i++){
		writer.write(',' + csv.headers[i]);
	}

	//body
	writer.write('\n');
	for(var i = 0; i < csv.data.length; i++){
		
		writer.write(csv.data[i][csv.headers[0]]);
		
		for(var j = 1; j < csv.headers.length; j++){
			writer.write(',' + csv.data[i][csv.headers[j]]);
		}
		writer.write('\n');
	}
	
	writer.end();

	writer.on('finish', function() {
	  cb();
	});
	
}

